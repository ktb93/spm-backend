<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    $res['success'] = true;
    $res['result'] = "Hello there welcome to web api using lumen tutorial!";
    return response($res);
});

$router->group(['middleware' => 'checkauthorization'], function () use ($router)
{	
    $router->post('login', 'Login\LoginController@LoginApp');
    $router->get('menu', 'Menu\MenuController@getMenuAll');

    //--Master Data
    $router->get('getCustomer', 'Customer\CustomerController@getCustomer');
    $router->get('custID', 'Customer\CustomerController@autoNumber');
    $router->post('storeCustomer', 'Customer\CustomerController@storeCustomer');
    $router->post('updateCustomer', 'Customer\CustomerController@updateCustomer');
    $router->post('deleteCustomer', 'Customer\CustomerController@deleteCustomer');

    $router->get('getAccType', 'AccountType\AccTypeController@getAccType');
    $router->get('accTypeID', 'AccountType\AccTypeController@autoNumber');
    $router->post('storeAccType', 'AccountType\AccTypeController@storeAccType');
    $router->post('updateAccType', 'AccountType\AccTypeController@updateAccType');
    $router->post('deleteAccType', 'AccountType\AccTypeController@deleteAccType');

    $router->get('getAccount', 'Account\AccountController@getAccount');
    $router->get('accountID', 'Account\AccountController@autoNumber');
    $router->post('storeAccount', 'Account\AccountController@storeAccount');
    $router->post('updateAccount', 'Account\AccountController@updateAccount');
    $router->post('deleteAccount', 'Account\AccountController@deleteAccount');
    $router->post('accTypeList', 'Account\AccountController@accTypeList');

    $router->get('getPosition', 'Position\PositionController@getPosition');
    $router->get('positionID', 'Position\PositionController@autoNumber');
    $router->post('storePosition', 'Position\PositionController@storePosition');
    $router->post('updatePosition', 'Position\PositionController@updatePosition');
    $router->post('deletePosition', 'Position\PositionController@deletePosition');

    $router->get('getUser', 'User\UserController@getUser');
    $router->get('userID', 'User\UserController@autoNumber');
    $router->post('storeUser', 'User\UserController@storeUser');
    $router->post('updateUser', 'User\UserController@updateUser');
    $router->post('deleteUser', 'User\UserController@deleteUser');
    $router->post('positionList', 'User\UserController@positionList');

    $router->post('getProject', 'Project\ProjectController@getProject');
    $router->get('projectID', 'Project\ProjectController@autoNumber');
    $router->post('detailProject', 'Project\ProjectController@detailProject');
    $router->post('storeProject', 'Project\ProjectController@storeProject');
    $router->post('updateProject', 'Project\ProjectController@editProject');
    $router->post('closeProject', 'Project\ProjectController@closeProject');
    $router->post('cancelProject', 'Project\ProjectController@cancelProject');

    $router->post('getContract', 'Contract\ContractController@getContract');
    $router->get('contractID', 'Contract\ContractController@autoNumber');
    $router->post('detailContract', 'Contract\ContractController@detailContract');
    $router->post('storeContract', 'Contract\ContractController@storeContract');
    $router->post('updateContract', 'Contract\ContractController@editContract');
    $router->get('getTmpContract', 'Contract\ContractController@getTmpContract');
    $router->get('tmpContractID', 'Contract\ContractController@autoTmpNumber');
    $router->post('tmpContract', 'Contract\ContractController@tmpContract');
    $router->post('delTmpContract', 'Contract\ContractController@delTmpContract');
    $router->post('closeContract', 'Contract\ContractController@closeContract');

    $router->post('getProjectList', 'Planning\PlanController@getProjectList');
    $router->get('getAccountList', 'Planning\PlanController@getAccountList');
    $router->post('getDetailPlan', 'Planning\PlanController@getDtlPlan');
    $router->get('planID', 'Planning\PlanController@autoNumber');
    $router->post('checkPlan', 'Planning\PlanController@checkPlan');
    $router->post('storePlan', 'Planning\PlanController@storePlan');
    $router->post('storeDetailPlan', 'Planning\PlanController@storeDetailPlan');
    $router->post('delDtlPlan', 'Planning\PlanController@deleteDtlPlan');

    $router->post('getProjectExp', 'Expense\ExpenseController@getProjectExp');
    $router->post('getDetailExpense', 'Expense\ExpenseController@getDtlExpense');
    $router->get('expenseID', 'Expense\ExpenseController@autoNumber');
    $router->post('checkExpense', 'Expense\ExpenseController@checkExpense');
    $router->post('storeExpense', 'Expense\ExpenseController@storeExpense');
    $router->post('storeDetailExpense', 'Expense\ExpenseController@storeDetailExpense');

    $router->post('getIncome', 'Income\IncomeController@getContractInc');
});

