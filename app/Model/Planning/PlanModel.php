<?php
namespace App\Model\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use App\Model\Project\ProjectModel;
use App\Model\Account\AccountModel;
use DB; 
use DataTables;

class PlanModel extends Model
{
    protected $table = 'tbl_rab';
    protected $connection = 'mysql1';

    public function getProjectList($request)
    {
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');

        $value = $request->input('search.value');
        $kd_proyek = ProjectModel::where('kd_proyek', 'like', '%'.$value.'%')->count();
        $nm_proyek = ProjectModel::where('nm_proyek', 'like', '%'.$value.'%')->count();
        $location = ProjectModel::where('location', 'like', '%'.$value.'%')->count();

        if($kd_proyek > 0){
            $where = 'kd_proyek like "%'.$value.'%"';
        }elseif($nm_proyek > 0){
            $where = 'nm_proyek like "%'.$value.'%"';
        }elseif($location > 0){
            $where = 'location like "%'.$value.'%"';
        }else{
            $where = 'kd_proyek like "%'.$value.'%"';
        }

        $project = ProjectModel::selectRaw('kd_proyek,nm_proyek,location,DATE_FORMAT(plan_start, "%d-%m-%Y") as plan_start,DATE_FORMAT(plan_finish, "%d-%m-%Y") as plan_finish, nilai_proyek, first_name, last_name')
                        ->join('tbl_user','tbl_user.id_user','=','tbl_proyek.pic')
                        ->where(function($q) use ($request){
                            if($request->startDate != '' && $request->endDate != ''){
                                $startDate = date("Y-m-d",strtotime($request->startDate));
                                $endDate = date("Y-m-d",strtotime($request->endDate));
                                $q->whereBetween($request->type, [$startDate,$endDate]);
                            }
                        })
                        ->where('status','Progress' )
                        ->whereRaw($where);
        
        $record['recordsTotal'] = $project->count();       
        $record['dataRecord'] = $project
                        ->orderBy($sort['col'], $sort['dir'])
                        ->skip($request->input('start'))
                        ->take($request->input('length',10))
                        ->get();

        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

    public function getAccountList($request)
    {
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');
        $account = AccountModel::join('tbl_akun_tipe','tbl_akun_tipe.kd_tipe_akun','=','tbl_akun.kd_tipe_akun')
                        ->where('kd_akun', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('nm_akun', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('tipe_akun', 'like', '%'.$request->input('search.value').'%');
        
        $record['recordsTotal'] = $account->count();       
        $record['dataRecord'] = $account
                        // ->orderBy($sort['col'], $sort['dir'])
                        ->skip($request->input('start'))
                        ->take($request->input('length',10))
                        ->get();

        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

    public function getDtlPlan($request)
    {
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');

        $plan = DB::connection('mysql1')->table('tbl_rab_detail as d')
                            ->selectRaw('d.kd_akun,a.nm_akun,a.kd_tipe_akun,t.tipe_akun,d.jml_rab,r.tgl_rab,r.total_rab,r.kd_rab')
                            ->join('tbl_rab as r', function($q) use ($request)
                            {
                                $q->on('r.kd_rab','=','d.kd_rab')
                                    ->where('r.kd_kontrak',$request->contractID);             
                            })
                            ->join('tbl_akun as a','a.kd_akun','=','d.kd_akun')
                            ->join('tbl_akun_tipe as t','t.kd_tipe_akun','=','a.kd_tipe_akun')
                            ->orWhere('a.nm_akun', 'like', '%'.$request->input('search.value').'%')
                            ->orWhere('t.tipe_akun', 'like', '%'.$request->input('search.value').'%')
                            ->orWhere('d.jml_rab', 'like', '%'.$request->input('search.value').'%');

        $record['recordsTotal'] = $plan->count();       
        $record['dataRecord'] = $plan->get();

        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

    public function planID($request){
        $query = Self::max('kd_rab');
        $code = substr($query,2,5);
        $nextCode = $code+1;
        $record = 'PL'.sprintf('%05s',$nextCode);
        return $record;
    }

    public function checkPlan($request){
        $query = Self::where('kd_kontrak',$request->contractID);
        $record['count'] = $query->count();
        $record['data'] = $query->first();

        return $record;
    }

    public function storePlan($request)
    {
        $planDate = date("Y-m-d");
        $plan = new PlanModel;
        $plan->kd_rab = $request->input('planID');
        $plan->kd_kontrak = $request->input('contractID');
        $plan->created_by = $request->input('userID');
        $plan->tgl_rab = $planDate;

        if($plan->save()){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function storeDetailPlan($request)
    {
        $createdAt = date("Y-m-d");
        $updatedAt = date("Y-m-d");

        $checkAccount = DB::connection('mysql1')->table('tbl_rab_detail')
                            ->where([
                                'kd_akun'=>$request->input('accountID'),'kd_rab'=>$request->input('planID')
                            ])->count();

        if($checkAccount == 0){
            $planVal = str_replace(',','',$request->input('planVal'));
            $plan = DB::connection('mysql1')->table('tbl_rab_detail')
                            ->insert([
                                'kd_rab' => $request->input('planID'),
                                'kd_akun' => $request->input('accountID'),
                                'created_by' => $request->input('userID'),
                                'jml_rab' => $planVal,
                                'created_at' => $createdAt,
                                'updated_at' => $updatedAt,
                            ]);
            
            $totalPlan = DB::connection('mysql1')->table('tbl_rab_detail')
                                ->where('kd_rab',$request->input('planID'))
                                ->sum('jml_rab');
            
            $updatePlan = Self::where('kd_rab',$request->input('planID'))
                                ->update(['total_rab' => $totalPlan]);

            if($plan==true){
                $record['success'] = true;
                $record['message'] = 'Success';
            }else{
                $record['success'] = false;
                $record['message'] = 'Failed';
            }
        }else{
            $record['success'] = false;
            $record['message'] = 'Account Already Exist In this Plan';

        }

        return $record;
    }

    public function deleteDtlPlan($request)
    {
        $plan = DB::connection('mysql1')->table('tbl_rab_detail')
                        ->where(['kd_rab'=>$request->planID,'kd_akun'=>$request->accountID])
                        ->delete();

        $totalPlan = DB::connection('mysql1')->table('tbl_rab_detail')
                        ->where('kd_rab',$request->input('planID'))
                        ->sum('jml_rab');
    
        $updatePlan = Self::where('kd_rab',$request->input('planID'))
                            ->update(['total_rab' => $totalPlan]);


        if($plan > 0){
            $record['success'] = true;
            $record['message'] = 'Data Account Deleted';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed To Delete Data';
        }

        return $record;
    }
}
