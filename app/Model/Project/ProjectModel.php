<?php
namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use App\Model\User\UserModel;
use DB; 
use DataTables;

class ProjectModel extends Model
{
    protected $table = 'tbl_proyek';
    protected $connection = 'mysql1';

    public function getProject($request)
    {

        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');

        $value = $request->input('search.value');
        $kd_proyek = Self::where('kd_proyek', 'like', '%'.$value.'%')->count();
        $nm_proyek = Self::where('nm_proyek', 'like', '%'.$value.'%')->count();
        $location = Self::where('location', 'like', '%'.$value.'%')->count();
        $status = Self::where('status', 'like', '%'.$value.'%')->count();

        if($kd_proyek > 0){
            $where = 'kd_proyek like "%'.$value.'%"';
        }elseif($nm_proyek > 0){
            $where = 'nm_proyek like "%'.$value.'%"';
        }elseif($location > 0){
            $where = 'location like "%'.$value.'%"';
        }elseif($status > 0){
            $where = 'status like "%'.$value.'%"';
        }else{
            $where = 'kd_proyek like "%'.$value.'%"';
        }

        $project = Self::selectRaw('kd_proyek,nm_proyek,location,DATE_FORMAT(plan_start, "%d-%m-%Y") as plan_start,DATE_FORMAT(plan_finish, "%d-%m-%Y") as plan_finish,status, IFNULL(CAST(((SELECT COUNT(*) FROM tbl_kontrak WHERE kd_proyek = tbl_proyek.kd_proyek and status="Finish")/(SELECT COUNT(*) FROM tbl_kontrak WHERE kd_proyek = tbl_proyek.kd_proyek and status!="Cancel"))*100 as int),0) AS progress')
                        ->distinct('kd_proyek')
                        ->where(function($q) use ($request){
                            if($request->startDate != '' && $request->endDate != ''){
                                $startDate = date("Y-m-d",strtotime($request->startDate));
                                $endDate = date("Y-m-d",strtotime($request->endDate));
                                $q->whereBetween($request->type, [$startDate,$endDate]);
                            }
                        })
                        ->whereRaw($where)
                        ->orderBy(DB::Raw("case when status='Progress' then 1 when status='Closed' then 2 else 3 end"));
        
        $record['recordsTotal'] = $project->count();       
        $record['dataRecord'] = $project
                        ->orderBy($sort['col'], $sort['dir'])
                        ->skip($request->input('start'))
                        ->take($request->input('length',10))
                        ->get();

        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

    public function projectID($request){
        $query = Self::max('kd_proyek');
        $code = substr($query,2,5);
        $nextCode = $code+1;
        $record = 'PR'.sprintf('%05s',$nextCode);
        return $record;
    }

    public function getDtlProject($request){
        $query = Self::selectRaw('*, IFNULL(CAST(((SELECT COUNT(*) FROM tbl_kontrak WHERE kd_proyek = tbl_proyek.kd_proyek and status="Finish")/(SELECT COUNT(*) FROM tbl_kontrak WHERE kd_proyek = tbl_proyek.kd_proyek  and status!="Cancel"))*100 as int),0) AS progress,first_name,last_name')
                    ->join('tbl_user', 'tbl_user.id_user','=','tbl_proyek.pic')
                    ->where('kd_proyek', '=', $request->kd_proyek)->get();
        $record['data'] = $query;
        return $record;
    }

    public function storeProject($request)
    {
        $totalProject = DB::connection('mysql1')->table('tmp_kontrak')
                            ->where('kd_proyek',$request->input('projectID'))
                            ->sum('nilai_kontrak');
        $planStart = date("Y-m-d",strtotime($request->input('planStart')));
        $planFinish = date("Y-m-d",strtotime($request->input('planFinish')));
        $project = new ProjectModel;
        $project->kd_proyek = $request->input('projectID');
        $project->nm_proyek = ucwords($request->input('project'));
        $project->location = ucwords($request->input('location'));
        $project->plan_start =  $planStart;
        $project->plan_finish = $planFinish;
        $project->note = $request->input('note');
        $project->pic = $request->input('pic');
        $project->nilai_proyek = $totalProject;

        if($project->save()){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function editProject($request)
    {
        $planStart = date("Y-m-d",strtotime($request->input('planStart')));
        $planFinish = date("Y-m-d",strtotime($request->input('planFinish')));
        $project = Self::where('kd_proyek',$request->input('projectID'))
                        ->update([
                            'nm_proyek' => ucwords($request->input('project')),
                            'location' => ucwords($request->input('location')),
                            'plan_start' => $planStart,
                            'plan_finish' => $planFinish,
                            'pic' => $request->input('userID'),
                            'note' => $request->input('note')
                        ]);
        if($project > 0){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function closeProject($request)
    {
        $actStart = date("Y-m-d",strtotime($request->input('actualStart')));
        $actFinish = date("Y-m-d",strtotime($request->input('actualFinish')));
        $project = Self::where('kd_proyek',$request->input('projectID'))
                        ->update([
                            'act_start' => $actStart,
                            'act_finish' => $actFinish,
                            'status' => 'Closed',
                            'closed_by' => $request->input('closedBy'),
                            'note' => $request->input('note')
                        ]);
        if($project > 0){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function cancelProject($request)
    {
        $project = Self::where('kd_proyek',$request->projectID)
                        ->update([
                            'status' => 'Cancel',
                            'cancel_by' => $request->cancelBy,
                            'note' => $request->note
                        ]);
        if($project > 0){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

}
