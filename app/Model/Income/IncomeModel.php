<?php
namespace App\Model\Income;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use DB; 
use DataTables;
use App\Model\Contract\ContractModel;

class IncomeModel extends Model
{
    protected $table = 'tbl_pendapatan';
    protected $connection = 'mysql1';
    
    public function getContractInc($request)
    {
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');

        $value = $request->value;


        // $value = $request->input('search.value');
        // $no_kontrak = ContractModel::where('no_kontrak', 'like', '%'.$value.'%')
        //                             ->join('tbl_pelanggan as c','c.kd_customer','=','tbl_kontrak.kd_customer')
        //                             ->leftjoin('tbl_pendapatan as t','t.kd_kontrak','=','tbl_kontrak.kd_kontrak')
        //                             ->count();
        // $desk_kontrak = ContractModel::where('desk_kontrak', 'like', '%'.$value.'%')
        //                             ->join('tbl_pelanggan as c','c.kd_customer','=','tbl_kontrak.kd_customer')
        //                             ->leftjoin('tbl_pendapatan as t','t.kd_kontrak','=','tbl_kontrak.kd_kontrak')
        //                             ->count();
        // $nm_customer = ContractModel::where('nm_customer', 'like', '%'.$value.'%')
        //                             ->join('tbl_pelanggan as c','c.kd_customer','=','tbl_kontrak.kd_customer')
        //                             ->leftjoin('tbl_pendapatan as t','t.kd_kontrak','=','tbl_kontrak.kd_kontrak')
        //                             ->count();
        // $nilai_kontrak = ContractModel::where('nilai_kontrak', 'like', '%'.$value.'%')
        //                             ->join('tbl_pelanggan as c','c.kd_customer','=','tbl_kontrak.kd_customer')
        //                             ->leftjoin('tbl_pendapatan as t','t.kd_kontrak','=','tbl_kontrak.kd_kontrak')
        //                             ->count();
        // $jml_pendapatan = ContractModel::where('jml_pendapatan', 'like', '%'.$value.'%')
        //                             ->join('tbl_pelanggan as c','c.kd_customer','=','tbl_kontrak.kd_customer')
        //                             ->leftjoin('tbl_pendapatan as t','t.kd_kontrak','=','tbl_kontrak.kd_kontrak')
        //                             ->count();
        // if($no_kontrak > 0){
        //     $where = "no_kontrak like '%123%'";
        // }elseif($desk_kontrak > 0){
        //     $where = 'desk_kontrak like "%'.$value.'%"';
        // }elseif($nm_customer > 0){
        //     $where = 'nm_customer like "%'.$value.'%"';
        // }elseif($nilai_kontrak > 0){
        //     $where = 'nilai_kontrak like "%'.$value.'%"';
        // }elseif($jml_pendapatan > 0){
        //     $where = 'jml_pendapatan like "%'.$value.'%"';
        // }else{
        //     $where = 'no_kontrak like "%'.$value.'%"';
        // }
        $where = 'no_kontrak like "%'.$value.'%"';

        $income = DB::connection('mysql1')->table('tbl_kontrak as k')
                        ->selectRaw('k.kd_kontrak,c.kd_customer,k.no_kontrak,k.desk_kontrak,c.nm_customer,k.nilai_kontrak,IFNULL(t.jml_pendapatan,0) as jml_pendapatan,IFNULL(k.nilai_kontrak-t.jml_pendapatan,k.nilai_kontrak) as balance')
                        ->join('tbl_proyek as p', function($q) use ($request)
                        {
                            $q->on('k.kd_proyek','=','p.kd_proyek')
                                ->where('k.kd_proyek','like', '%'.$request->projectID.'%');  
                        })
                        ->join('tbl_pelanggan as c','c.kd_customer','=','k.kd_customer')
                        ->leftjoin('tbl_pendapatan as t','k.kd_kontrak','=','t.kd_kontrak')
                        ->whereRaw($where);

        // if($income->count() == 0){
        //     $where = 'desk_kontrak like "%'.$value.'%"';
        //     if($income->count() == 0){
        //         $where = 'nm_customer like "%'.$value.'%"';
        //         if($income->count() == 0){
        //             $where = 'nilai_kontrak like "%'.$value.'%"';
        //             if($income->count() == 0){
        //                 $where = 'jml_pendapatan like "%'.$value.'%"';
        //             }
        //         }
        //     }
        // }

        dd($income->count());
        
        // $income = $data.'where no_kontrak like "%'.$value.'%"';
        
        $record['recordsTotal'] = $income->count();       
        $record['dataRecord'] = $income
                        // ->orderBy($sort['col'], $sort['dir'])
                        ->skip($request->input('start'))
                        ->take($request->input('length',10))
                        ->get();

        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

}
