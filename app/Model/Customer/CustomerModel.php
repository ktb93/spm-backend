<?php
namespace App\Model\Customer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use DB; 
use DataTables;

class CustomerModel extends Model
{
    protected $table = 'tbl_pelanggan';
    protected $connection = 'mysql1';
    public $timestamps = false;
    
    public function getCustomer($request)
    {
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');
        $customer = Self::where('kd_customer', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('nm_customer', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('address', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('phone', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('pic', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('email', 'like', '%'.$request->input('search.value').'%');
        
        $record['recordsTotal'] = $customer->count();       
        $record['dataRecord'] = $customer
                        ->orderBy($sort['col'], $sort['dir'])
                        ->skip($request->input('start'))
                        ->take($request->input('length',10))
                        ->get();

        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

    public function customerID($request){
        $query = Self::max('kd_customer');
        $code = substr($query,1,4);
        $nextCode = $code+1;
        $record = 'C'.sprintf('%04s',$nextCode);
        return $record;
    }

    public function storeCustomer($request)
    {
        $customer = new CustomerModel;
        $customer->kd_customer = $request->input('custID');
        $customer->nm_customer = ucwords($request->input('custName'));
        $customer->address = ucwords($request->input('address'));
        $customer->phone = $request->input('phone');
        $customer->pic = ucwords($request->input('pic'));
        $customer->email = $request->input('email');
        if($customer->save()){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function updateCustomer($request)
    {
        $customer = Self::where('kd_customer',$request->input('custID'))
                        ->update([
                            'nm_customer' => ucwords($request->input('custName')),
                            'address' => ucwords($request->input('address')),
                            'phone' => $request->input('phone'),
                            'pic' => ucwords($request->input('pic')),
                            'email' => $request->input('email')
                        ]);
        if($customer > 0){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function deleteCustomer($request)
    {
        $customer = Self::where('kd_customer',$request->custID)
                        ->delete();

        if($customer > 0){
            $record['success'] = true;
            $record['message'] = 'Data Customer Deleted';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed To Delete Data';
        }

        return $record;
    }
 
}
