<?php
namespace App\Model\Menu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB; 

class MenuModel extends Model
{
    protected $table = 'tbl_menu as a';
    protected $connection = 'mysql1';
    public $timestamps = false;
    
    public function getMenuAll($parent, $level, $url, $request)
    {
        $menu = [[]];
        $key=0;

        $data = Self::selectRaw(' distinct a.*, b.Count')
				->join(DB::raw('(SELECT id_main_menu, COUNT(*) AS Count  FROM tbl_menu GROUP BY id_main_menu) as b '), 'a.id_menu', '=', 'b.id_main_menu', 'left outer')
                ->where('a.id_main_menu', $parent)
                ->orderby('priority','asc')
                ->get();

        foreach ($data as $data2)
        {
            if(strpos($data2->link, '#') !== false)
            {
                $pisah = explode('#',$data2->link);
                $link = $pisah[1];
            }
            else
            {
                $link = $data2->link;
            }
            if ($data2->Count > 0) {
                if(strpos($url, $link) !== false)
                {		
                        $menu[$key]['id_menu']=$data2->id_menu ;
                        $menu[$key]['Name']=$data2->menu_name  ;
                        $menu[$key]['Link']=$data2->link  ;
                        $menu[$key]['Icon']=$data2->icon  ;
                        $menu[$key]['Submenu']="0"  ;
                        $menu[$key]['Level']=$level ;
                }
                else
                {	
                    $menu[$key]['id_menu']=$data2->id_menu ;
                    $menu[$key]['Name']=$data2->menu_name  ;
                    $menu[$key]['Link']=$data2->link  ;
                    $menu[$key]['Icon']=$data2->icon  ;
                    $menu[$key]['Submenu']="0"  ;
                    $menu[$key]['Level']=$level ;
                }
                $key++;
                $menu2=$this->getAllMenu2($data2->id_menu, $level+1, $url, $request);

                foreach ($menu2 as $row) {
                    try{
                    $menu[$key]['id_menu']=$row['id_menu']; 
                    $menu[$key]['Name'] =$row['menu_name'];
                    $menu[$key]['Link']=$row['link'];
                    $menu[$key]['Icon']=$row['icon'];
                    $menu[$key]['Submenu']=$row['Submenu']; 
                    $menu[$key]['Level']=$row['Level'] ;
                    $key++;
                    }catch(\Exception $e){
                        $menu[$key-1]['Submenu']="0"  ;
                    }
                }
                
            } elseif ($data2->Count==0) {
                if(strpos($url, $link) !== false)
                {	 
                    $menu[$key]['id_menu']=$data2->id_menu ;
                    $menu[$key]['Name']=$data2->menu_name  ;
                    $menu[$key]['Link']=$data2->link  ;
                    $menu[$key]['Icon']=$data2->icon  ;
                    $menu[$key]['Submenu']="0"  ;
                    $menu[$key]['Level']=$level ;
                }
                else
                {   $menu[$key]['id_menu']=$data2->id_menu ;
                    $menu[$key]['Name']=$data2->menu_name  ;
                    $menu[$key]['Link']=$data2->link  ;
                    $menu[$key]['Icon']=$data2->icon  ;
                    $menu[$key]['Submenu']="0"  ;
                    $menu[$key]['Level']=$level ;
                }
            } else{}
            $key++;
        }
        return $menu ;   
    }

    public  function getAllMenu2($parent, $level, $url , $request)
    {	
        $menu = [[]];
        $key=0;
    
        $data = MenuModel::selectRaw(' distinct a.*, b.Count')
				->join(DB::raw('(SELECT id_main_menu, COUNT(*) AS Count  FROM tbl_menu GROUP BY id_main_menu) as b '), 'a.id_menu', '=', 'b.id_main_menu', 'left outer')
                ->where('a.id_main_menu', $parent)
                ->orderby('priority','asc')
				->get();
            // echo "<ul>";

            foreach ($data as $data2)
            {
                if(strpos($data2->link, '#') !== false)
                {
                    //$link = $data2->LINK;
                    $pisah = explode('#',$data2->link);
                    $link = $pisah[1];
                }
                else
                {
                    $link = $data2->link;
                }
                if ($data2->Count > 0) {
                    if(strpos($url, $link) !== false)
                    {	$menu[$key]['id_menu']=$data2->id_menu ;
                        $menu[$key]['menu_name']=$data2->menu_name  ;
                        $menu[$key]['link']=$data2->link  ;
                        $menu[$key]['icon']=$data2->icon  ;
                        $menu[$key]['Submenu']="1"  ;
                        $menu[$key]['Level']=$level  ;
                    }
                    else
                    {	$menu[$key]['id_menu']=$data2->id_menu ;
                        $menu[$key]['menu_name']=$data2->menu_name  ;
                        $menu[$key]['link']=$data2->link  ;
                        $menu[$key]['icon']=$data2->icon  ;
                        $menu[$key]['Submenu']="1"  ;
                        $menu[$key]['Level']=$level  ;
                    }
                    $key++;
                    $menu2=$this->getAllMenu2($data2->ID_MENU, $level+1, $url,$request);
                    //echo "</ul></li>";
                    foreach ($menu2 as $row) {
                        $menu[$key]['id_menu']=$row['id_menu'];
                        $menu[$key]['menu_name']=$data2->menu_name  ;
                        $menu[$key]['link']=$data2->link  ;
                        $menu[$key]['icon']=$data2->icon  ;
                        $menu[$key]['Submenu']="1"  ;
                        $menu[$key]['Level']=$level  ;
                        $key++;
                    }
    
                } elseif ($data2->Count==0) {
                    if(strpos($url, $link) !== false)
                    {	$menu[$key]['id_menu']=$data2->id_menu ;
                        $menu[$key]['menu_name']=$data2->menu_name  ;
                        $menu[$key]['link']=$data2->link  ;
                        $menu[$key]['icon']=$data2->icon  ;
                        $menu[$key]['Submenu']="1"  ;
                        $menu[$key]['Level']=$level  ;
                    }
                    else
                    {	$menu[$key]['id_menu']=$data2->id_menu ;
                        $menu[$key]['menu_name']=$data2->menu_name  ;
                        $menu[$key]['link']=$data2->link  ;
                        $menu[$key]['icon']=$data2->icon  ;
                        $menu[$key]['Submenu']="1"  ;
                        $menu[$key]['Level']=$level  ;
                    }
    
                } else{}
                $key++;
            }
        // echo "</ul>";
        return $menu ;   
    }
 
}
