<?php
namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use App\Model\Position\PositionModel;
use DB; 
use DataTables;

class UserModel extends Model
{
    protected $table = 'tbl_user';
    protected $connection = 'mysql1';
    public $timestamps = false;

    public function getUser($request)
    {
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');
        $user = Self::join('tbl_position','tbl_position.id_position','=','tbl_user.id_position')
                        ->where('id_user', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('first_name', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('last_name', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('username', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('password', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('position', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('email', 'like', '%'.$request->input('search.value').'%');
        $record['recordsTotal'] = $user->count();       
        $record['dataRecord'] = $user
                        ->orderBy($sort['col'], $sort['dir'])
                        ->skip($request->input('start'))
                        ->take($request->input('length',10))
                        ->get();
        // dd($record['dataRecord']);
        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

    public function userID($request){
        $query = Self::max('id_user');
        $code = substr($query,1,4);
        $nextCode = $code+1;
        $record = '1'.sprintf('%04s',$nextCode);
        return $record;
    }

    public function storeUser($request)
    {
        $user = new UserModel;
        $user->id_user = $request->input('userID');
        $user->first_name = ucwords($request->input('firstname'));
        $user->last_name = ucwords($request->input('lastname'));
        $user->username = $request->input('username');
        $user->password = $request->input('password');
        $user->id_position = $request->input('position');
        $user->email = $request->input('email');
        if($user->save()){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function updateUser($request)
    {
        $user = Self::where('id_user',$request->input('userID'))
                        ->update([
                            'first_name' => ucwords($request->input('firstname')),
                            'last_name' => ucwords($request->input('lastname')),
                            'username' => $request->input('username'),
                            'id_position' => $request->input('position'),
                            'email' => $request->input('email')
                        ]);
        if($user > 0){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function deleteUser($request)
    {
        $user = Self::where('id_user',$request->userID)
                        ->delete();

        if($user > 0){
            $record['success'] = true;
            $record['message'] = 'Data User Deleted';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed To Delete Data';
        }

        return $record;
    }

    public function positionList($request)
    {
        $position = PositionModel::where('id_position','!=', $request->posID)->orderBy('id_position')->get();
        $record['recordsTotal'] = $position->count();       
        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $position;

        return $record;
    }

}
