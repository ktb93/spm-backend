<?php
namespace App\Model\Position;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use App\Model\User\UserModel;
use DB; 
use DataTables;

class PositionModel extends Model
{
    protected $table = 'tbl_position';
    protected $connection = 'mysql1';
    public $timestamps = false;

    public function getPosition($request)
    {
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');
        $position = Self::where('id_position', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('position', 'like', '%'.$request->input('search.value').'%');
        
        $record['recordsTotal'] = $position->count();       
        $record['dataRecord'] = $position
                        ->orderBy($sort['col'], $sort['dir'])
                        ->skip($request->input('start'))
                        ->take($request->input('length',10))
                        ->get();

        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

    public function positionID($request){
        $query = Self::max('id_position');
        $code = substr($query,1,2);
        $nextCode = $code+1;
        $record = 'P'.sprintf('%02s',$nextCode);
        return $record;
    }

    public function storePosition($request)
    {
        $position = new PositionModel;
        $position->id_position = $request->input('posID');
        $position->position = ucwords($request->input('position'));
        if($position->save()){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function updatePosition($request)
    {
        $position = Self::where('id_position',$request->input('posID'))
                        ->update([
                            'position' => ucwords($request->input('position'))
                        ]);
        if($position > 0){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function deletePosition($request)
    {
        $position = Self::where('id_position',$request->posID)
                        ->delete();

        if($position > 0){
            $record['success'] = true;
            $record['message'] = 'Data Position Deleted';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed To Delete Data';
        }

        return $record;
    }
 
}
