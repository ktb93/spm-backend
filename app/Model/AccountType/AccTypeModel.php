<?php
namespace App\Model\AccountType;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use DB; 
use DataTables;

class AccTypeModel extends Model
{
    protected $table = 'tbl_akun_tipe';
    protected $connection = 'mysql1';
    public $timestamps = false;
    
    public function getAccType($request)
    {
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');
        $accType = Self::where('kd_tipe_akun', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('tipe_akun', 'like', '%'.$request->input('search.value').'%');
        
        $record['recordsTotal'] = $accType->count();       
        $record['dataRecord'] = $accType
                        ->orderBy($sort['col'], $sort['dir'])
                        ->skip($request->input('start'))
                        ->take($request->input('length',10))
                        ->get();

        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

    public function accTypeID($request){
        $query = Self::max('kd_tipe_akun');
        $code = substr($query,2,3);
        $nextCode = $code+1;
        $record = 'AT'.sprintf('%03s',$nextCode);
        return $record;
    }

    public function storeAccType($request)
    {
        $accType = new AccTypeModel;
        $accType->kd_tipe_akun = $request->input('accTypeID');
        $accType->tipe_akun = ucwords($request->input('accType'));
        if($accType->save()){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function updateAccType($request)
    {
        $accType = Self::where('kd_tipe_akun',$request->input('accTypeID'))
                        ->update([
                            'tipe_akun' => ucwords($request->input('accType'))
                        ]);
        if($accType > 0){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function deleteAccType($request)
    {
        $accType = Self::where('kd_tipe_akun',$request->accTypeID)
                        ->delete();

        if($accType > 0){
            $record['success'] = true;
            $record['message'] = 'Data Account Type Deleted';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed To Delete Type Data';
        }

        return $record;
    }
}
