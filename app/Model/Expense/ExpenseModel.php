<?php
namespace App\Model\Expense;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use App\Model\Project\ProjectModel;
use App\Model\Account\AccountModel;
use App\Model\Planning\PlanModel;
use DB; 
use DataTables;

class ExpenseModel extends Model
{
    protected $table = 'tbl_biaya';
    protected $connection = 'mysql1';

    public function getProjectExp($request)
    {
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');

        $value = $request->input('search.value');
        $kd_proyek = ProjectModel::where('kd_proyek', 'like', '%'.$value.'%')->count();
        $nm_proyek = ProjectModel::where('nm_proyek', 'like', '%'.$value.'%')->count();
        $location = ProjectModel::where('location', 'like', '%'.$value.'%')->count();

        if($kd_proyek > 0){
            $where = 'p.kd_proyek like "%'.$value.'%"';
        }elseif($nm_proyek > 0){
            $where = 'p.nm_proyek like "%'.$value.'%"';
        }elseif($location > 0){
            $where = 'p.location like "%'.$value.'%"';
        }else{
            $where = 'p.kd_proyek like "%'.$value.'%"';
        }

        $project = DB::connection('mysql1')->table('tbl_proyek as p')
                        ->selectRaw('p.kd_proyek,p.nm_proyek,p.location,DATE_FORMAT(p.plan_start, "%d-%m-%Y") as plan_start,DATE_FORMAT(p.plan_finish, "%d-%m-%Y") as plan_finish, p.nilai_proyek, u.first_name, u.last_name')
                        ->join('tbl_user as u','u.id_user','=','p.pic')
                        ->join('tbl_kontrak as k','k.kd_proyek','=','p.kd_proyek')
                        ->join('tbl_rab as r','r.kd_kontrak','=','k.kd_kontrak')
                        ->distinct('k.kd_proyek')
                        ->where(function($q) use ($request){
                            if($request->startDate != '' && $request->endDate != ''){
                                $startDate = date("Y-m-d",strtotime($request->startDate));
                                $endDate = date("Y-m-d",strtotime($request->endDate));
                                $q->whereBetween($request->type, [$startDate,$endDate]);
                            }
                        })
                        ->where('p.status','Progress' )
                        ->whereRaw($where);

        $record['recordsTotal'] = $project->count('k.kd_proyek');
        
        $record['dataRecord'] = $project
                        ->orderBy($sort['col'], $sort['dir'])
                        ->skip($request->input('start'))
                        ->take($request->input('length',10))
                        ->get();

        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

    public function getDtlExpense($request)
    {
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');

        $expense = DB::connection('mysql1')->table('tbl_biaya_detail as d')
                            ->selectRaw('d.kd_akun,a.nm_akun,a.kd_tipe_akun,t.tipe_akun,y.jml_rab,d.jml_biaya,r.tgl_biaya,r.total_biaya,r.kd_biaya,y.jml_rab-d.jml_biaya as balance')
                            ->join('tbl_biaya as r', function($q) use ($request)
                            {
                                $q->on('r.kd_biaya','=','d.kd_biaya')
                                    ->where('r.kd_kontrak',$request->contractID);             
                            })
                            ->join('tbl_rab_detail as y','y.kd_akun','=','d.kd_akun')
                            ->join('tbl_rab as x', function($q) use ($request)
                            {
                                $q->on('x.kd_rab','=','y.kd_rab')
                                    ->where('x.kd_kontrak',$request->contractID);             
                            })
                            ->join('tbl_akun as a','a.kd_akun','=','d.kd_akun')
                            ->join('tbl_akun_tipe as t','t.kd_tipe_akun','=','a.kd_tipe_akun')
                            ->orWhere('a.nm_akun', 'like', '%'.$request->input('search.value').'%')
                            ->orWhere('t.tipe_akun', 'like', '%'.$request->input('search.value').'%')
                            ->orWhere('d.jml_biaya', 'like', '%'.$request->input('search.value').'%');

        $record['recordsTotal'] = $expense->count();       
        $record['dataRecord'] = $expense->get();

        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

    public function expenseID($request){
        $query = Self::max('kd_biaya');
        $code = substr($query,2,5);
        $nextCode = $code+1;
        $record = 'EX'.sprintf('%05s',$nextCode);
        return $record;
    }

    public function checkExpense($request){
        $query = Self::where('kd_kontrak',$request->contractID);
        $record['count'] = $query->count();
        $record['data'] = $query->first();

        return $record;
    }

    public function storeExpense($request)
    {
        $today = date("Y-m-d");
        $expense = new ExpenseModel;
        $expense->kd_biaya = $request->input('expenseID');
        $expense->kd_kontrak = $request->input('contractID');
        $expense->created_by = $request->input('userID');
        $expense->tgl_biaya = $today;

        if($expense->save()){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        $detailExpense = PlanModel::join('tbl_rab_detail', 'tbl_rab.kd_rab','=','tbl_rab_detail.kd_rab')
                                ->where('kd_kontrak', $request->input('contractID'))
                                ->get();

        $countDetail = $detailExpense->count();
        
        for($i = 0; $i < $countDetail; $i++){
            $saveDetail = DB::connection('mysql1')->table('tbl_biaya_detail')
                                ->insert([
                                    'kd_biaya' => $request->input('expenseID'),
                                    'kd_akun' => $detailExpense[$i]->kd_akun,
                                    'created_by' => $request->input('userID'),
                                    'created_at' => $today,
                                    'updated_at' => $today,
                                ]);
        }

        if($saveDetail == false){
            ExpenseModel::where('kd_biaya',$request->input('expenseID'))->delete();
            $record['success'] = false;
            $record['message'] = 'Failed';                    
        }

        return $record;
    }

    public function storeDetailExpense($request)
    {
        $today = date("Y-m-d");
        $biaya = str_replace(',','',$request->biaya);
        $updateExpense = DB::connection('mysql1')->table('tbl_biaya_detail as d')
                            ->join('tbl_biaya as b', function($q) use ($request)
                            {
                                $q->on('b.kd_biaya','=','d.kd_biaya')
                                    ->where('b.kd_kontrak',$request->contractID);             
                            })
                            ->where('d.kd_akun',$request->accountID)
                            ->update([
                                'jml_biaya' => $biaya,
                                'd.updated_at' => $today
                            ]);

        if($updateExpense==true){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }
}
