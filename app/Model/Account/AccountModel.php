<?php
namespace App\Model\Account;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use App\Model\AccountType\AccTypeModel;
use DB; 
use DataTables;

class AccountModel extends Model
{
    protected $table = 'tbl_akun';
    protected $connection = 'mysql1';
    public $timestamps = false;
    
    public function getAccount($request)
    {
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');
        $account = Self::join('tbl_akun_tipe','tbl_akun_tipe.kd_tipe_akun','=','tbl_akun.kd_tipe_akun')
                        ->where('kd_akun', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('nm_akun', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('tipe_akun', 'like', '%'.$request->input('search.value').'%');
        
        $record['recordsTotal'] = $account->count();       
        $record['dataRecord'] = $account
                        ->orderBy($sort['col'], $sort['dir'])
                        ->skip($request->input('start'))
                        ->take($request->input('length',10))
                        ->get();

        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

    public function accountID($request){
        $query = Self::max('kd_akun');
        $code = substr($query,1,4);
        $nextCode = $code+1;
        $record = 'A'.sprintf('%04s',$nextCode);
        return $record;
    }

    public function storeAccount($request)
    {
        $checkAccount = Self::where('nm_akun',$request->input('accName'))->count();
        if ($checkAccount == 0){
            $account = new AccountModel;
            $account->kd_akun = $request->input('accID');
            $account->nm_akun = ucwords($request->input('accName'));
            $account->kd_tipe_akun = $request->input('accType');
            if($account->save()){
                $record['success'] = true;
                $record['message'] = 'Success';
            }else{
                $record['success'] = false;
                $record['message'] = 'Failed';
            }
        }else{
            $record['success'] = false;
            $record['message'] = 'Account Already Exist';

        }
        
        return $record;
    }

    public function updateAccount($request)
    {
        $account = Self::where('kd_akun',$request->input('accID'))
                        ->update([
                            'nm_akun' => ucwords($request->input('accName')),
                            'kd_tipe_akun' => ucwords($request->input('accType'))
                        ]);
        if($account > 0){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function deleteAccount($request)
    {
        $account = Self::where('kd_akun',$request->accID)
                        ->delete();

        if($account > 0){
            $record['success'] = true;
            $record['message'] = 'Data Account Deleted';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed To Delete Data';
        }

        return $record;
    }

    public function accTypeList($request)
    {
        $position = AccTypeModel::where('kd_tipe_akun','!=', $request->accTypeID)->orderBy('kd_tipe_akun')->get();
        $record['recordsTotal'] = $position->count();       
        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $position;

        return $record;
    }
}
