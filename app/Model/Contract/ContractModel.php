<?php
namespace App\Model\Contract;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use App\Model\Project\ProjectModel;
use DB; 
use DataTables;

class ContractModel extends Model
{
    protected $table = 'tbl_kontrak';
    protected $connection = 'mysql1';
    
    public function getContract($request)
    {
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');
        $contract = DB::connection('mysql1')->table('tbl_kontrak as k')
                        ->selectRaw('k.kd_kontrak,k.no_kontrak,k.desk_kontrak,c.kd_customer,c.nm_customer,k.nilai_kontrak,k.deadline,k.status,p.kd_proyek,p.nm_proyek')
                        ->join('tbl_proyek as p', function($q) use($request){
                                $q->on('p.kd_proyek','=','k.kd_proyek')
                                    ->where('k.kd_proyek', $request->projectID);
                        })
                        ->join('tbl_pelanggan as c','c.kd_customer','=','k.kd_customer')                        
                        ->where('k.kd_kontrak', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('k.no_kontrak', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('k.desk_kontrak', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('p.nm_proyek', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('c.nm_customer', 'like', '%'.$request->input('search.value').'%');
 
        $record['recordsTotal'] = $contract->count();       
        $record['dataRecord'] = $contract
                        // ->orderBy($sort['col'], $sort['dir'])
                        ->get();

        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

    public function contractID($request){
        $query = Self::max('kd_kontrak');
        $code = substr($query,2,5);
        $nextCode = $code+1;
        $record = 'CO'.sprintf('%05s',$nextCode);
        return $record;
    }

    public function getDtlContract($request){
        $query = DB::connection('mysql1')->table('tbl_kontrak as k')
                    ->selectRaw('k.kd_kontrak,k.no_kontrak,k.desk_kontrak,c.kd_customer,c.nm_customer,k.nilai_kontrak,k.deadline,k.status,p.kd_proyek,p.nm_proyek')
                    ->where('k.kd_kontrak', '=', $request->kd_kontrak)                       
                    ->join('tbl_pelanggan as c','c.kd_customer','=','k.kd_customer')
                    ->join('tbl_proyek as p','p.kd_proyek','=','k.kd_proyek')->get();

        $record['data'] = $query;
        return $record;
    }

    public function storeContract($request)
    {
        $contractVal = str_replace(',','',$request->input('contractVal'));
        $deadline = date("Y-m-d",strtotime($request->input('deadline')));
        $contract = new ContractModel;
        $contract->kd_kontrak = $request->input('contractID');
        $contract->no_kontrak = $request->input('contractNo');
        $contract->desk_kontrak = ucwords($request->input('contract'));
        $contract->kd_customer =  $request->input('custID');
        $contract->nilai_kontrak = $contractVal;
        $contract->deadline = $deadline;
        $contract->kd_proyek = $request->input('projectID');

        if($contract->save()){
            $totalProject = self::where('kd_proyek',$request->input('projectID'))
                                ->sum('nilai_kontrak');
            $updateProject = ProjectModel::where('kd_proyek',$request->input('projectID'))
                                        ->update([
                                            'nilai_proyek' => $totalProject
                                        ]);

            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function editContract($request)
    {
        $contractVal = str_replace(',','',$request->input('contractVal-E'));
        $deadline = date("Y-m-d",strtotime($request->input('deadline-E')));
        $contract = Self::where('kd_kontrak',$request->input('contractID-E'))
                        ->update([
                            'no_kontrak' => $request->input('contractNo-E'),
                            'desk_kontrak' => ucwords($request->input('contract-E')),
                            'kd_customer' => $request->input('custID-E'),
                            'nilai_kontrak' => $contractVal,
                            'deadline' => $deadline
                        ]);
        if($contract > 0){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function closeContract($request)
    {
        $closeDate = date('Y-m-d');
        if ($request->status != 'Finish'){
            $contract = Self::where('kd_kontrak',$request->contractID)
            ->update([
                'status' => ucwords($request->status)
            ]);
        }else{
            $contract = Self::where('kd_kontrak',$request->contractID)
            ->update([
                'status' => ucwords($request->status),
                'closed_at' => $closeDate
            ]);
        }


        if($contract > 0){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function getTmpContract($request)
    {
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');
        $tmpContract = DB::connection('mysql1')->table('tmp_kontrak')
                        ->join('tbl_pelanggan','tbl_pelanggan.kd_customer','=','tmp_kontrak.kd_customer')
                        ->orWhere('no_kontrak', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('desk_kontrak', 'like', '%'.$request->input('search.value').'%')                ->orWhere('nm_customer', 'like', '%'.$request->input('search.value').'%')
                        ->orWhere('nilai_kontrak', 'like', '%'.$request->input('search.value').'%');

        $record['recordsTotal'] = $tmpContract->count();       
        $record['dataRecord'] = $tmpContract
                        ->orderBy($sort['col'], $sort['dir'])
                        ->get();

        $record['recordsFiltered'] = $record['recordsTotal'];
        $record['data'] = $record['dataRecord'];
        $record['draw'] = intval($request->input('draw'));

        return $record;
    }

    public function contractTmpID($request){
        $query = DB::connection('mysql1')->table('tmp_kontrak')->max('kd_kontrak');
        $code = substr($query,2,5);
        $nextCode = $code+1;
        $record = 'CO'.sprintf('%05s',$nextCode);
        return $record;
    }

    public function tmpContract($request)
    {
        $contractVal = str_replace(',','',$request->input('contractVal'));
        $deadline = date("Y-m-d",strtotime($request->input('deadline')));
        $tmpContract = DB::connection('mysql1')->table('tmp_kontrak')->insert([
                                'kd_kontrak' => $request->input('contractID'),
                                'no_kontrak' => $request->input('contractNo'),
                                'desk_kontrak' => ucwords($request->input('contract')),
                                'kd_customer' => $request->input('custID'),
                                'nilai_kontrak' => $contractVal,
                                'deadline' => $deadline,
                                'kd_proyek' => $request->input('projectID')
                            ]);
        if($tmpContract){
            $record['success'] = true;
            $record['message'] = 'Success';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed';
        }

        return $record;
    }

    public function delTmpContract($request)
    {
        $tmpCustomer = DB::connection('mysql1')->table('tmp_kontrak')
                        ->where('kd_kontrak',$request->contractID)
                        ->delete();

        if($tmpCustomer > 0){
            $record['success'] = true;
            $record['message'] = 'Data Contract Deleted';
        }else{
            $record['success'] = false;
            $record['message'] = 'Failed To Delete Data';
        }

        return $record;
    }

}
