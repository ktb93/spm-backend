<?php
namespace App\Model\Login;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use DB; 

class LoginModel extends Model
{
    protected $table = 'tbl_user as u';
    protected $connection = 'mysql1';
    public $timestamps = false;
    
    public function loginApp($request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

		$user = Self::whereRaw("username ='".$username."' and password ='".$password."'")->first();
		$passwordsha1 = sha1($password);
        $hasher = app()->make('hash');

		if($user) 
        {
            try {
                $api_token = sha1(config('aplikasi.salt').time().$user->id_user);
                $record['success'] = true;
                $record['api_token'] = $api_token;
                $record['message'] = $user;
                $record['password_encrypt'] = $passwordsha1;
                return $record;
            }
            catch (Exception $e) {
                $record['success'] = false;
				$record['message'] = 'error';
				return $record;
            }

        }
        else
        {
            $record['success'] = false;
            $record['message'] = 'Your username or password incorrect!';
            return $record;
        }
    }
 
}
