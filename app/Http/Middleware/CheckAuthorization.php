<?php

namespace App\Http\Middleware;

use Closure;

class CheckAuthorization
{
    public function handle($request, Closure $next)
    {
		$headerAuth = $request->header('Authorization');
        
        if(!empty($headerAuth)){
            if($headerAuth == "Basic Yml6bmV0OmJpem5ldA=="){
                $res['success'] = 200;
                $res['message'] = 'Success';
                $res['headerAuth'] = $request->header('Authorization');
            }else{
                $res['code'] = 401;
                $res['message'] = 'Access Denied. Invalid Authorization';
                $res['headerAuth'] = $headerAuth;
                return response($res);
            }           
        }else{
            $rest['code'] = 400;
            $rest['message'] = 'Authorization is misssing';
            return $rest;
        }

        return $next($request);
    }
}
