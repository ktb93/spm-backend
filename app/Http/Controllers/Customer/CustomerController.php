<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customer\CustomerModel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use DB;

class CustomerController extends Controller
{
    public function getCustomer(Request $request)
    {	
        $data = new CustomerModel;
        $record=$data->getCustomer($request);
        if(isset($record) && $record['recordsTotal'] > 0)
        {
            $res['success'] = true;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
        else
        {
            $res['success'] = false;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
		return response($res);
    }

    public function autoNumber(Request $request){
        $data =  new CustomerModel;
        $record=$data->customerID($request);
        $res['customerID'] = $record;
        return response($res);
    }	

    public function storeCustomer(Request $request)
    {	
        $data = new CustomerModel;
        $record=$data->storeCustomer($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }else{
            $res['success'] = false;
            $res['message'] = $record['message'];
        }

		return response($res);
    }

    public function updateCustomer(Request $request)
    {	
        $data = new CustomerModel;
        $record=$data->updateCustomer($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }

    public function deleteCustomer(Request $request)
    {	
        $data = new CustomerModel;
        $record=$data->deleteCustomer($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }

}