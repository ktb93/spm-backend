<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User\UserModel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use DB;

class UserController extends Controller
{
    public function getUser(Request $request)
    {	
        $data = new UserModel;
        $record=$data->getUser($request);
        if(isset($record) && $record['recordsTotal'] > 0)
        {
            $res['success'] = true;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
        else
        {
            $res['success'] = false;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
		return response($res);
    }

    public function autoNUmber(Request $request){
        $data =  new UserModel;
        $record=$data->userID($request);
        $res['userID'] = $record;
        return response($res);
    }	

    public function storeUser(Request $request)
    {	
        $data = new UserModel;
        $record=$data->storeUser($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }else{
            $res['success'] = false;
            $res['message'] = $record['message'];
        }

		return response($res);
    }

    public function updateUser(Request $request)
    {	
        $data = new UserModel;
        $record=$data->updateUser($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }

    public function deleteUser(Request $request)
    {	
        $data = new UserModel;
        $record=$data->deleteUser($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }

    public function positionList(Request $request)
    {	
        $data = new UserModel;
        $record=$data->positionList($request);
        if(isset($record) && $record['recordsTotal'] > 0)
        {
            $res['success'] = true;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['data'] = $record['data'];
        }
        else
        {
            $res['success'] = false;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['data'] = $record['data'];
        }
		return response($res);
    }
}