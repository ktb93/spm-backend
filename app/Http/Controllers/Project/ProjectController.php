<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Project\ProjectModel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use DB;

class ProjectController extends Controller
{
    public function getProject(Request $request)
    {	
        $data = new ProjectModel;
        $record=$data->getProject($request);
        if(isset($record) && $record['recordsTotal'] > 0)
        {
            $res['success'] = true;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
        else
        {
            $res['success'] = false;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
		return response($res);
    }

    public function autoNumber(Request $request){
        $data =  new ProjectModel;
        $record=$data->projectID($request);
        $res['projectID'] = $record;
        return response($res);
    }
    
    public function detailProject(Request $request){
        $data =  new ProjectModel;
        $record=$data->getDtlProject($request);
        $res['data'] = $record['data'];
        return response($res);
    }

    public function storeProject(Request $request)
    {	
        $data = new ProjectModel;
        $record=$data->storeProject($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }else{
            $res['success'] = false;
            $res['message'] = $record['message'];
        }

		return response($res);
    }

    public function editProject(Request $request)
    {	
        $data = new ProjectModel;
        $record=$data->editProject($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }

    public function closeProject(Request $request)
    {	
        $data = new ProjectModel;
        $record=$data->closeProject($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }

    public function cancelProject(Request $request)
    {	
        $data = new ProjectModel;
        $record=$data->cancelProject($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }
}