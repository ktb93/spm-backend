<?php

namespace App\Http\Controllers\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Menu\MenuModel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class MenuController extends Controller
{
	public function getMenuAll(Request $request) {
        $menu = new MenuModel;
		$menuv=$menu->getMenuAll(0,1,'',$request);
		$res['success'] = true;
		$res['message'] = $menuv;
		return response($res);
	}	

}