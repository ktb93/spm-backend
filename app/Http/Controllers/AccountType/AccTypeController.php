<?php

namespace App\Http\Controllers\AccountType;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AccountType\AccTypeModel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use DB;

class AccTypeController extends Controller
{
    public function getAccType(Request $request)
    {	
        $data = new AccTypeModel;
        $record=$data->getAccType($request);
        if(isset($record) && $record['recordsTotal'] > 0)
        {
            $res['success'] = true;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
        else
        {
            $res['success'] = false;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
		return response($res);
    }

    public function autoNumber(Request $request){
        $data =  new AccTypeModel;
        $record=$data->accTypeID($request);
        $res['accTypeID'] = $record;
        return response($res);
    }	

    public function storeAccType(Request $request)
    {	
        $data = new AccTypeModel;
        $record=$data->storeAccType($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }else{
            $res['success'] = false;
            $res['message'] = $record['message'];
        }

		return response($res);
    }

    public function updateAccType(Request $request)
    {	
        $data = new AccTypeModel;
        $record=$data->updateAccType($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }

    public function deleteAccType(Request $request)
    {	
        $data = new AccTypeModel;
        $record=$data->deleteAccType($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }

}