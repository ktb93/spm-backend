<?php

namespace App\Http\Controllers\Login;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Login\LoginModel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use DB;

class LoginController extends Controller
{
    public function loginApp(Request $request)
    {	
        $data = new LoginModel;
        $record=$data->loginApp($request);
        if(isset($record) && $record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
            $res['api_token'] = $record['api_token'];
            $res['password_encrypt'] = $record['password_encrypt'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }

	public function logout(Request $request)
    {
    	$EMPLOYEE_ID = $request->input('EMPLOYEE_ID');
        DB::connection('sqlsrv')
            ->table(DB::Raw('intranet2017.dbo.TBL_LOGIN_ACCESS'))
            ->where('EMPLOYEE_ID', $EMPLOYEE_ID )
            ->where('APP_NAME', '=', 'Intranet')	
            ->update(['TOKEN_ID' => '']);
	}
}