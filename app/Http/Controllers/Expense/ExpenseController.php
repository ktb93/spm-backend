<?php

namespace App\Http\Controllers\Expense;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Expense\ExpenseModel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use DB;

class ExpenseController extends Controller
{
    public function getProjectExp(Request $request)
    {	
        $data = new ExpenseModel;
        $record=$data->getProjectExp($request);
        if(isset($record) && $record['recordsTotal'] > 0)
        {
            $res['success'] = true;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
        else
        {
            $res['success'] = false;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
		return response($res);
    }

    public function getDtlExpense(Request $request)
    {	
        $data = new ExpenseModel;
        $record=$data->getDtlExpense($request);
        if(isset($record) && $record['recordsTotal'] > 0)
        {
            $res['success'] = true;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
        else
        {
            $res['success'] = false;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
		return response($res);
    }

    public function autoNumber(Request $request){
        $data =  new ExpenseModel;
        $record=$data->expenseID($request);
        $res['expenseID'] = $record;
        return response($res);
    }

    public function checkExpense(Request $request){
        $data =  new ExpenseModel;
        $record=$data->checkExpense($request);
        if($record['count'] > 0){
            $res['status'] = true;
            $res['data'] = $record['data'];
        }else{
            $res['status'] = false;
        }

        return response($res);
    }
    
    public function storeExpense(Request $request)
    {	
        $data = new ExpenseModel;
        $record=$data->storeExpense($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }else{
            $res['success'] = false;
            $res['message'] = $record['message'];
        }

		return response($res);
    }

    public function storeDetailExpense(Request $request)
    {	
        $data = new ExpenseModel;
        $record=$data->storeDetailExpense($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }else{
            $res['success'] = false;
            $res['message'] = $record['message'];
        }

		return response($res);
    }
}