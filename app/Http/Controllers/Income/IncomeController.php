<?php

namespace App\Http\Controllers\Income;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Income\IncomeModel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use DB;

class IncomeController extends Controller
{
    public function getContractInc(Request $request)
    {	
        $data = new IncomeModel;
        $record=$data->getContractInc($request);
        if(isset($record) && $record['recordsTotal'] > 0)
        {
            $res['success'] = true;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
        else
        {
            $res['success'] = false;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
		return response($res);
    }

}