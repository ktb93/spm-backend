<?php

namespace App\Http\Controllers\Contract;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Contract\ContractModel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use DB;

class ContractController extends Controller
{
    public function getContract(Request $request)
    {	
        $data = new ContractModel;
        $record=$data->getContract($request);
        if(isset($record) && $record['recordsTotal'] > 0)
        {
            $res['success'] = true;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
        else
        {
            $res['success'] = false;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
		return response($res);
    }

    public function autoNumber(Request $request){
        $data =  new ContractModel;
        $record=$data->contractID($request);
        $res['contractID'] = $record;
        return response($res);
    }
    
    public function detailContract(Request $request){
        $data =  new ContractModel;
        $record=$data->getDtlContract($request);
        $res['data'] = $record['data'];
        return response($res);
    }

    public function storeContract(Request $request)
    {	
        $data = new ContractModel;
        $record=$data->storeContract($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }else{
            $res['success'] = false;
            $res['message'] = $record['message'];
        }

		return response($res);
    }

    public function editContract(Request $request)
    {	
        $data = new ContractModel;
        $record=$data->editContract($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }

    public function closeContract(Request $request)
    {	
        $data = new ContractModel;
        $record=$data->closeContract($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }

    public function getTmpContract(Request $request)
    {	
        $data = new ContractModel;
        $record=$data->getTmpContract($request);
        if(isset($record) && $record['recordsTotal'] > 0)
        {
            $res['success'] = true;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
        else
        {
            $res['success'] = false;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
		return response($res);
    }

    public function autoTmpNumber(Request $request){
        $data =  new ContractModel;
        $record = $data->contractTmpID($request);
        $res['contractID'] = $record;
        return response($res);
    }

    public function tmpContract(Request $request)
    {	
        $data = new ContractModel;
        $record=$data->tmpContract($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }else{
            $res['success'] = false;
            $res['message'] = $record['message'];
        }

		return response($res);
    }

    public function delTmpContract(Request $request)
    {	
        $data = new ContractModel;
        $record=$data->delTmpContract($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }
}