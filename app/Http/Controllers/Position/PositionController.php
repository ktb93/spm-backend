<?php

namespace App\Http\Controllers\Position;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Position\PositionModel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use DB;

class PositionController extends Controller
{
    public function getPosition(Request $request)
    {	
        $data = new PositionModel;
        $record=$data->getPosition($request);
        if(isset($record) && $record['recordsTotal'] > 0)
        {
            $res['success'] = true;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
        else
        {
            $res['success'] = false;
            $res['recordsTotal'] = $record['recordsTotal'];
            $res['recordsFiltered'] = $record['recordsFiltered'];
            $res['draw'] = $record['draw'];
            $res['data'] = $record['data'];
        }
		return response($res);
    }

    public function autoNumber(Request $request){
        $data =  new PositionModel;
        $record=$data->positionID($request);
        $res['positionID'] = $record;
        return response($res);
    }	

    public function storePosition(Request $request)
    {	
        $data = new PositionModel;
        $record=$data->storePosition($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }else{
            $res['success'] = false;
            $res['message'] = $record['message'];
        }

		return response($res);
    }

    public function updatePosition(Request $request)
    {	
        $data = new PositionModel;
        $record=$data->updatePosition($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }

    public function deletePosition(Request $request)
    {	
        $data = new PositionModel;
        $record=$data->deletePosition($request);
        if($record['success'] == true)
        {
            $res['success'] = true;
            $res['message'] = $record['message'];
        }
        else
        {
            $res['success'] = false;
            $res['message'] = $record['message'];
        }
		return response($res);
    }

}