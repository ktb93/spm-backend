<?php

return [
    'apiUrl' => 'http://localhost/spb-backend/public/',
    'salt' => 'garem',
    'codeRedis' => 'key_',
    'cookieExpired' => 600,
    'cookieExpiredBranch' => 43200,
    'menuExpired' => 10080
];
