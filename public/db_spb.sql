-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2018 at 12:12 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_spb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_akun`
--

CREATE TABLE `tbl_akun` (
  `kd_akun` varchar(5) NOT NULL,
  `nm_akun` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_akun`
--

INSERT INTO `tbl_akun` (`kd_akun`, `nm_akun`) VALUES
('A0002', 'Konsumsi'),
('A0003', 'Jalan Raya');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_biaya`
--

CREATE TABLE `tbl_biaya` (
  `kd_biaya` varchar(10) NOT NULL,
  `kd_proyek` varchar(7) NOT NULL,
  `tgl_biaya` date NOT NULL,
  `total_biaya` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `kd_customer` varchar(5) NOT NULL,
  `nm_customer` varchar(20) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `pic` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`kd_customer`, `nm_customer`, `address`, `phone`, `pic`, `email`) VALUES
('C0022', 'PT Indonesia Testing', 'Indo', '0857-4949-0631', 'Wawan', 'ccc@gmail.com'),
('C0023', 'PT Indonesia Testing', 'indo', '085749490631', 'wawan', 'ccc@gmail.com'),
('C0024', 'PT Indonesia Testing', 'indo', '085749490631', 'wawan', 'ccc@gmail.com'),
('C0025', 'PT Indonesia Testing', 'indo', '085749490631', 'wawan', 'ccc@gmail.com'),
('C0026', 'PT P3K', 'indo', '085749490631', 'wawan', 'aaa@aaa.gmail'),
('C0027', 'PT Indonesia', 'indo', '085749490631', 'wawan', 'indonesia@gmail.com'),
('C0028', 'PT Purpa Asin', 'indo', '085749490631', 'wawan', 'purpa@gmail.com'),
('C0029', 'PT Akas', 'indo', '085749490631', 'wawan', 'akas@gmail.com'),
('C0030', 'PT Reda', 'indo', '085749490631', 'wawan', 'reda@gmail.com'),
('C0031', 'PT Tirta Mas', 'indo', '085749490631', 'wawan', 'tirta@gmai.com'),
('C0032', 'PT Andara', 'indo', '085749490631', 'wawan', 'andara@gmail.com'),
('C0033', 'PT Papan', 'jl. papan', '0851111988', 'joko', 'joko@gmail.com'),
('C0034', 'PT Haha', 'Jl. Haha Indo', '0857382992', 'Riyan', 'riyan@gmail.com'),
('C0035', 'PT Papandayan', 'Jl. Papandayan', '08119199199', 'Riyan', 'riyan@gmail.com'),
('C0036', 'PT Tukang Baso', 'Jl. Tukang Baso', '08109287383', 'Pupu', 'preman@gmail.com'),
('C0037', 'PT Pupus', 'Jl. Pupus', '0857-4948-2838', 'Henro', 'henro@gmail.com'),
('C0038', 'PT Harga Diri', 'Jl. Indonesia Raya', '0828-2882-8288', 'Usus', 'usus@gmail.com'),
('C0039', 'PT Sukses Jaya', 'Jl. Papandayan', '0859-2920-000', 'Peron', 'peron@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_biaya`
--

CREATE TABLE `tbl_detail_biaya` (
  `kd_detail_biaya` varchar(10) NOT NULL,
  `kd_biaya` varchar(10) NOT NULL,
  `kd_akun` varchar(5) NOT NULL,
  `jml_biaya` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_rab`
--

CREATE TABLE `tbl_detail_rab` (
  `kd_detail_rab` varchar(10) NOT NULL,
  `kd_rab` varchar(10) NOT NULL,
  `kd_akun` varchar(5) NOT NULL,
  `jml_rab` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kontrak`
--

CREATE TABLE `tbl_kontrak` (
  `kd_kontrak` varchar(7) NOT NULL,
  `kd_proyek` varchar(7) NOT NULL,
  `no_kontrak` varchar(20) NOT NULL,
  `desk_kontrak` varchar(100) NOT NULL,
  `kd_customer` varchar(5) NOT NULL,
  `nilai_kontrak` double NOT NULL,
  `status` varchar(10) DEFAULT 'Progress',
  `deadline` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kontrak`
--

INSERT INTO `tbl_kontrak` (`kd_kontrak`, `kd_proyek`, `no_kontrak`, `desk_kontrak`, `kd_customer`, `nilai_kontrak`, `status`, `deadline`) VALUES
('CO00001', 'PR00001', '1234', 'Indonesia Maju', 'C0022', 1313, 'Progress', NULL),
('CO00002', 'PR00001', '21313', 'Indonesia Maju', 'C0022', 12313, 'Progress', NULL),
('CO00003', 'PR00002', '123123', '12313', 'C0022', 12313, 'Finish', NULL),
('CO00004', 'PR00001', '1234', 'Indonesia Maju', 'C0022', 1313, 'Finish', NULL),
('CO00005', 'PR00001', '1234', 'Indonesia Maju', 'C0022', 1313, 'Finish', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id_menu` int(11) NOT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `menu_description` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `id_main_menu` char(2) DEFAULT NULL,
  `priority` char(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id_menu`, `menu_name`, `menu_description`, `link`, `icon`, `id_main_menu`, `priority`) VALUES
(1, 'Master Data', 'Master Data', '#A', 'bars', '0', '1'),
(2, 'Customer Data', 'Customer Data', 'customerList', 'cubes', '1', '11'),
(3, 'Project Data', 'Project Data', 'project', 'cubes', '7', '11'),
(4, 'Account Data', 'Account Data', 'accountList', 'cubes', '1', '12'),
(5, 'Contract Data', 'Contract Data', '#E', 'cubes', '7', '12'),
(6, 'Users Data', 'Users Data', 'userList', 'cubes', '1', '14'),
(7, 'Transaction Data', 'Transaction Data', '#G', 'tasks', '0', '2'),
(8, 'Income Transaction', 'Income Transaction', '#G', 'cubes', '7', '13'),
(9, 'Expanse Transaction', 'Expanse Transaction', '#G', 'cubes', '7', '14'),
(10, 'Report', 'Report', '#G', 'bar-chart-o', '0', '3'),
(11, 'Daily Report', 'Daily Report', '#G', 'cubes', '10', '11'),
(12, 'Profit and Loss', 'Profit and Loss', '#G', 'cubes', '10', '12'),
(13, 'Position Data', 'Position Data', 'positionList', 'cubes', '1', '13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pendapatan`
--

CREATE TABLE `tbl_pendapatan` (
  `kd_pendapatan` varchar(7) NOT NULL,
  `no_inv` varchar(13) NOT NULL,
  `kd_kontrak` varchar(7) NOT NULL,
  `tgl_pendapatan` date NOT NULL,
  `jml_pendapatan` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_position`
--

CREATE TABLE `tbl_position` (
  `id_position` char(3) NOT NULL,
  `position` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_position`
--

INSERT INTO `tbl_position` (`id_position`, `position`) VALUES
('P01', 'Manager Operasional'),
('P02', 'Project Manager'),
('P03', 'Admin Pusat'),
('P04', 'Admin Project'),
('P05', 'Manager');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project`
--

CREATE TABLE `tbl_project` (
  `kd_proyek` varchar(7) NOT NULL,
  `nm_proyek` varchar(20) NOT NULL,
  `location` varchar(100) NOT NULL,
  `plan_start` date NOT NULL,
  `act_start` date DEFAULT NULL,
  `plan_finish` date NOT NULL,
  `act_finish` date DEFAULT NULL,
  `status` varchar(50) DEFAULT 'Progress',
  `note` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_project`
--

INSERT INTO `tbl_project` (`kd_proyek`, `nm_proyek`, `location`, `plan_start`, `act_start`, `plan_finish`, `act_finish`, `status`, `note`) VALUES
('PR00001', 'Indonesia', 'Jawa Barat', '2018-12-27', '2018-12-12', '2018-12-31', '2019-12-12', 'Progress', 'testing'),
('PR00002', 'Indonesia Mundur', 'Bandung', '2018-12-21', NULL, '2019-12-21', NULL, 'Progress', ''),
('PR00003', 'Indonesia Mundur', 'Bandung', '2018-12-21', NULL, '2019-12-21', NULL, 'Progress', ''),
('PR00004', 'Hidup Indonesia', 'Jakarta', '2018-12-08', NULL, '2018-12-15', NULL, 'Progress', NULL),
('PR00005', 'Indonesiaku', 'Bandung', '2018-12-14', NULL, '2018-12-28', NULL, 'Progress', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rab`
--

CREATE TABLE `tbl_rab` (
  `kd_rab` varchar(10) NOT NULL,
  `kd_proyek` varchar(7) NOT NULL,
  `tgl_rab` date NOT NULL,
  `total_rab` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` char(5) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(50) NOT NULL,
  `id_position` char(5) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `first_name`, `last_name`, `username`, `password`, `id_position`, `email`) VALUES
('10001', 'Administrator', 'Administrator', 'admin', 'admin', 'P01', 'riyanalfian93@gmail.com'),
('10002', 'user', 'user', 'user', 'user', 'P03', 'riyanalfian93@gmail.com'),
('10003', 'Wawan', 'Yusuf', 'wawan123', 'indonesia', 'P01', 'wawan@gmai.com'),
('10004', 'Riyan', 'Alfian', 'riyan93', 'advance123', 'P01', 'riyan@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_kontrak`
--

CREATE TABLE `tmp_kontrak` (
  `kd_kontrak` varchar(7) NOT NULL,
  `kd_proyek` varchar(7) NOT NULL,
  `no_kontrak` varchar(20) NOT NULL,
  `desk_kontrak` varchar(100) NOT NULL,
  `kd_customer` varchar(5) NOT NULL,
  `nilai_kontrak` double NOT NULL,
  `deadline` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmp_kontrak`
--

INSERT INTO `tmp_kontrak` (`kd_kontrak`, `kd_proyek`, `no_kontrak`, `desk_kontrak`, `kd_customer`, `nilai_kontrak`, `deadline`) VALUES
('CO00006', 'PR00006', 'test', 'Test', 'C0024', 5000000, '2018-12-31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_akun`
--
ALTER TABLE `tbl_akun`
  ADD PRIMARY KEY (`kd_akun`);

--
-- Indexes for table `tbl_biaya`
--
ALTER TABLE `tbl_biaya`
  ADD PRIMARY KEY (`kd_biaya`),
  ADD KEY `fk_biaya_project` (`kd_proyek`) USING BTREE;

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`kd_customer`);

--
-- Indexes for table `tbl_detail_biaya`
--
ALTER TABLE `tbl_detail_biaya`
  ADD PRIMARY KEY (`kd_detail_biaya`),
  ADD KEY `fk_dtlbiaya_biaya` (`kd_biaya`) USING BTREE,
  ADD KEY `fk_dtlbiaya_akun` (`kd_akun`) USING BTREE;

--
-- Indexes for table `tbl_detail_rab`
--
ALTER TABLE `tbl_detail_rab`
  ADD PRIMARY KEY (`kd_detail_rab`),
  ADD KEY `fk_dtlrab_rab` (`kd_rab`) USING BTREE,
  ADD KEY `fk_dtlrab_akun` (`kd_akun`) USING BTREE;

--
-- Indexes for table `tbl_kontrak`
--
ALTER TABLE `tbl_kontrak`
  ADD PRIMARY KEY (`kd_kontrak`),
  ADD KEY `fk_kontrak_proyek` (`kd_proyek`),
  ADD KEY `fk_kontrak_customer` (`kd_customer`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `tbl_pendapatan`
--
ALTER TABLE `tbl_pendapatan`
  ADD PRIMARY KEY (`kd_pendapatan`),
  ADD KEY `fk_pendapatan_kontrak` (`kd_kontrak`);

--
-- Indexes for table `tbl_position`
--
ALTER TABLE `tbl_position`
  ADD PRIMARY KEY (`id_position`);

--
-- Indexes for table `tbl_project`
--
ALTER TABLE `tbl_project`
  ADD PRIMARY KEY (`kd_proyek`);

--
-- Indexes for table `tbl_rab`
--
ALTER TABLE `tbl_rab`
  ADD PRIMARY KEY (`kd_rab`),
  ADD KEY `fk_rab_proyek` (`kd_proyek`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tmp_kontrak`
--
ALTER TABLE `tmp_kontrak`
  ADD PRIMARY KEY (`kd_kontrak`),
  ADD KEY `kd_customer` (`kd_customer`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_biaya`
--
ALTER TABLE `tbl_biaya`
  ADD CONSTRAINT `fk_project` FOREIGN KEY (`kd_proyek`) REFERENCES `tbl_project` (`kd_proyek`);

--
-- Constraints for table `tbl_detail_biaya`
--
ALTER TABLE `tbl_detail_biaya`
  ADD CONSTRAINT `fk_akun` FOREIGN KEY (`kd_akun`) REFERENCES `tbl_akun` (`kd_akun`),
  ADD CONSTRAINT `fk_biaya` FOREIGN KEY (`kd_biaya`) REFERENCES `tbl_biaya` (`kd_biaya`);

--
-- Constraints for table `tbl_detail_rab`
--
ALTER TABLE `tbl_detail_rab`
  ADD CONSTRAINT `fk_rab` FOREIGN KEY (`kd_rab`) REFERENCES `tbl_rab` (`kd_rab`),
  ADD CONSTRAINT `fk_rab_akun` FOREIGN KEY (`kd_akun`) REFERENCES `tbl_akun` (`kd_akun`);

--
-- Constraints for table `tbl_kontrak`
--
ALTER TABLE `tbl_kontrak`
  ADD CONSTRAINT `fk_kontrak_customer` FOREIGN KEY (`kd_customer`) REFERENCES `tbl_customer` (`kd_customer`),
  ADD CONSTRAINT `fk_kontrak_proyek` FOREIGN KEY (`kd_proyek`) REFERENCES `tbl_project` (`kd_proyek`);

--
-- Constraints for table `tbl_pendapatan`
--
ALTER TABLE `tbl_pendapatan`
  ADD CONSTRAINT `fk_pendapatan_kontrak` FOREIGN KEY (`kd_kontrak`) REFERENCES `tbl_kontrak` (`kd_kontrak`);

--
-- Constraints for table `tbl_rab`
--
ALTER TABLE `tbl_rab`
  ADD CONSTRAINT `fk_rab_proyek` FOREIGN KEY (`kd_proyek`) REFERENCES `tbl_project` (`kd_proyek`);

--
-- Constraints for table `tmp_kontrak`
--
ALTER TABLE `tmp_kontrak`
  ADD CONSTRAINT `tmp_kontrak_ibfk_1` FOREIGN KEY (`kd_proyek`) REFERENCES `tbl_project` (`kd_proyek`),
  ADD CONSTRAINT `tmp_kontrak_ibfk_2` FOREIGN KEY (`kd_customer`) REFERENCES `tbl_customer` (`kd_customer`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
