-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2019 at 10:35 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_spb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_akun`
--

CREATE TABLE `tbl_akun` (
  `kd_akun` varchar(5) NOT NULL,
  `nm_akun` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_akun`
--

INSERT INTO `tbl_akun` (`kd_akun`, `nm_akun`) VALUES
('A0002', 'Konsumsi'),
('A0003', 'Jalan Raya');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_biaya`
--

CREATE TABLE `tbl_biaya` (
  `kd_biaya` varchar(10) NOT NULL,
  `kd_proyek` varchar(7) NOT NULL,
  `tgl_biaya` date NOT NULL,
  `total_biaya` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_biaya`
--

CREATE TABLE `tbl_detail_biaya` (
  `kd_detail_biaya` varchar(10) NOT NULL,
  `kd_biaya` varchar(10) NOT NULL,
  `kd_akun` varchar(5) NOT NULL,
  `jml_biaya` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_rab`
--

CREATE TABLE `tbl_detail_rab` (
  `kd_detail_rab` varchar(10) NOT NULL,
  `kd_rab` varchar(10) NOT NULL,
  `kd_akun` varchar(5) NOT NULL,
  `jml_rab` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kontrak`
--

CREATE TABLE `tbl_kontrak` (
  `kd_kontrak` varchar(7) NOT NULL,
  `kd_proyek` varchar(7) NOT NULL,
  `no_kontrak` varchar(20) NOT NULL,
  `desk_kontrak` varchar(100) NOT NULL,
  `kd_customer` varchar(5) NOT NULL,
  `nilai_kontrak` double NOT NULL,
  `status` varchar(10) DEFAULT 'Progress',
  `deadline` date DEFAULT NULL,
  `closed_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kontrak`
--

INSERT INTO `tbl_kontrak` (`kd_kontrak`, `kd_proyek`, `no_kontrak`, `desk_kontrak`, `kd_customer`, `nilai_kontrak`, `status`, `deadline`, `closed_at`, `created_at`, `updated_at`) VALUES
('CO00001', 'PR00001', '1234', 'Indonesia Maju', 'C0022', 1313, 'Progress', NULL, NULL, NULL, NULL),
('CO00002', 'PR00001', '21313', 'Indonesia Maju', 'C0022', 75000000, 'Progress', NULL, NULL, NULL, NULL),
('CO00003', 'PR00002', '123123', '12313', 'C0022', 50000000, 'Finish', NULL, NULL, NULL, NULL),
('CO00004', 'PR00001', '1234', 'Indonesia Maju', 'C0022', 4000000, 'Finish', NULL, NULL, NULL, NULL),
('CO00005', 'PR00001', '1234', 'Indonesia Maju', 'C0022', 1313, 'Finish', NULL, NULL, NULL, NULL),
('CO00006', 'PR00009', '1', '1', 'C0025', 1, 'Finish', '2019-01-08', NULL, NULL, NULL),
('CO00007', 'PR00009', '2', '2', 'C0027', 2, 'Finish', '2019-01-16', NULL, NULL, NULL),
('CO00008', 'PR00009', '3', '3', 'C0029', 3, 'Progress', '2019-01-17', NULL, NULL, NULL),
('CO00009', 'PR00021', 'test/12/12', 'Papayon', 'C0022', 4000000, 'Finish', '2019-01-09', NULL, NULL, NULL),
('CO00010', 'PR00021', 'loro/loro', 'Susu', 'C0024', 5000000, 'Cancel', '2019-01-23', NULL, NULL, NULL),
('CO00011', 'PR00022', '12/12/12', 'Papan', 'C0023', 4000000, 'Progress', '2019-01-17', NULL, NULL, NULL),
('CO00012', 'PR00022', '12/12/12', 'Papan', 'C0023', 500000, 'Progress', '2019-01-16', NULL, NULL, NULL),
('CO00013', 'PR00025', '12/12', 'Ind', 'C0022', 5000000, 'Progress', '2019-01-11', NULL, NULL, NULL),
('CO00014', 'PR00025', '16/12', 'Kun', 'C0026', 9000000, 'Progress', '2019-01-17', NULL, NULL, NULL),
('CO00015', 'PR00025', '10/100', 'Rendah', 'C0023', 500000, 'Progress', '2019-01-18', NULL, '2019-01-10', '2019-01-10'),
('CO00016', 'PR00025', '1000/1', 'UpDown', 'C0025', 2000000, 'Progress', '2019-01-17', NULL, '2019-01-10', '2019-01-10'),
('CO00017', 'PR00025', 'plan/', 'Planning', 'C0027', 3000000, 'Progress', '2019-01-17', NULL, '2019-01-10', '2019-01-10'),
('CO00018', 'PR00025', 'test', 'Tste', 'C0026', 50000, 'Progress', '2019-01-17', NULL, '2019-01-10', '2019-01-10'),
('CO00019', 'PR00025', 'permen', 'Permen', 'C0029', 1000000, 'Progress', '2019-01-17', NULL, '2019-01-10', '2019-01-10'),
('CO00020', 'PR00025', 'tes2', 'Test2', 'C0025', 1000000, 'Progress', '2019-01-17', NULL, '2019-01-10', '2019-01-10'),
('CO00021', 'PR00025', 'tes3', 'Test3', 'C0026', 2000000, 'Progress', '2019-01-18', NULL, '2019-01-10', '2019-01-10'),
('CO00022', 'PR00025', 'tes4', 'Tes4', 'C0028', 5000000, 'Progress', '2019-01-18', NULL, '2019-01-10', '2019-01-10');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id_menu` int(11) NOT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `menu_description` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `id_main_menu` char(2) DEFAULT NULL,
  `priority` char(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id_menu`, `menu_name`, `menu_description`, `link`, `icon`, `id_main_menu`, `priority`) VALUES
(1, 'Master Data', 'Master Data', '#A', 'bars', '0', '1'),
(2, 'Customer Data', 'Customer Data', 'customerList', 'cubes', '1', '11'),
(3, 'Project Data', 'Project Data', 'project', 'cubes', '7', '11'),
(4, 'Account Data', 'Account Data', 'accountList', 'cubes', '1', '12'),
(6, 'Users Data', 'Users Data', 'userList', 'cubes', '1', '14'),
(7, 'Transaction Data', 'Transaction Data', '#G', 'tasks', '0', '2'),
(8, 'Income Transaction', 'Income Transaction', '#G', 'cubes', '7', '12'),
(9, 'Expanse Transaction', 'Expanse Transaction', '#G', 'cubes', '7', '13'),
(10, 'Report', 'Report', '#G', 'bar-chart-o', '0', '3'),
(11, 'Daily Report', 'Daily Report', '#G', 'cubes', '10', '11'),
(12, 'Profit and Loss', 'Profit and Loss', '#G', 'cubes', '10', '12'),
(13, 'Position Data', 'Position Data', 'positionList', 'cubes', '1', '13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pelanggan`
--

CREATE TABLE `tbl_pelanggan` (
  `kd_customer` varchar(5) NOT NULL,
  `nm_customer` varchar(20) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `pic` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pelanggan`
--

INSERT INTO `tbl_pelanggan` (`kd_customer`, `nm_customer`, `address`, `phone`, `pic`, `email`) VALUES
('C0022', 'PT Indonesia Testing', 'Indo', '0857-4949-0631', 'Wawan', 'ccc@gmail.com'),
('C0023', 'PT Indonesia Testing', 'indo', '085749490631', 'wawan', 'ccc@gmail.com'),
('C0024', 'PT Indonesia Testing', 'indo', '085749490631', 'wawan', 'ccc@gmail.com'),
('C0025', 'PT Indonesia Testing', 'indo', '085749490631', 'wawan', 'ccc@gmail.com'),
('C0026', 'PT P3K', 'indo', '085749490631', 'wawan', 'aaa@aaa.gmail'),
('C0027', 'PT Indonesia', 'indo', '085749490631', 'wawan', 'indonesia@gmail.com'),
('C0028', 'PT Purpa Asin', 'indo', '085749490631', 'wawan', 'purpa@gmail.com'),
('C0029', 'PT Akas', 'indo', '085749490631', 'wawan', 'akas@gmail.com'),
('C0030', 'PT Reda', 'indo', '085749490631', 'wawan', 'reda@gmail.com'),
('C0031', 'PT Tirta Mas', 'indo', '085749490631', 'wawan', 'tirta@gmai.com'),
('C0032', 'PT Andara', 'indo', '085749490631', 'wawan', 'andara@gmail.com'),
('C0033', 'PT Papan', 'jl. papan', '0851111988', 'joko', 'joko@gmail.com'),
('C0034', 'PT Haha', 'Jl. Haha Indo', '0857382992', 'Riyan', 'riyan@gmail.com'),
('C0035', 'PT Papandayan', 'Jl. Papandayan', '08119199199', 'Riyan', 'riyan@gmail.com'),
('C0036', 'PT Tukang Baso', 'Jl. Tukang Baso', '08109287383', 'Pupu', 'preman@gmail.com'),
('C0037', 'PT Pupus', 'Jl. Pupus', '0857-4948-2838', 'Henro', 'henro@gmail.com'),
('C0038', 'PT Harga Diri', 'Jl. Indonesia Raya', '0828-2882-8288', 'Usus', 'usus@gmail.com'),
('C0039', 'PT Sukses Jaya', 'Jl. Papandayan', '0859-2920-000', 'Peron', 'peron@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pendapatan`
--

CREATE TABLE `tbl_pendapatan` (
  `kd_pendapatan` varchar(7) NOT NULL,
  `no_inv` varchar(13) NOT NULL,
  `kd_kontrak` varchar(7) NOT NULL,
  `tgl_pendapatan` date NOT NULL,
  `jml_pendapatan` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_position`
--

CREATE TABLE `tbl_position` (
  `id_position` char(3) NOT NULL,
  `position` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_position`
--

INSERT INTO `tbl_position` (`id_position`, `position`) VALUES
('P01', 'Manager Operasional'),
('P02', 'Project Manager'),
('P03', 'Admin Pusat'),
('P04', 'Admin Project'),
('P05', 'Manager');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_proyek`
--

CREATE TABLE `tbl_proyek` (
  `kd_proyek` varchar(7) NOT NULL,
  `nm_proyek` varchar(50) NOT NULL,
  `location` varchar(100) NOT NULL,
  `plan_start` date NOT NULL,
  `act_start` date DEFAULT NULL,
  `plan_finish` date NOT NULL,
  `act_finish` date DEFAULT NULL,
  `status` varchar(10) DEFAULT 'Progress',
  `note` text,
  `nilai_proyek` double DEFAULT NULL,
  `pic` char(5) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `closed_by` char(5) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `cancel_by` char(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_proyek`
--

INSERT INTO `tbl_proyek` (`kd_proyek`, `nm_proyek`, `location`, `plan_start`, `act_start`, `plan_finish`, `act_finish`, `status`, `note`, `nilai_proyek`, `pic`, `created_at`, `closed_by`, `updated_at`, `cancel_by`) VALUES
('PR00001', 'Indonesia', 'Jawa Barat', '2018-12-27', '2018-12-12', '2018-12-31', '2019-12-12', 'Progress', 'testing', 0, '10001', NULL, NULL, NULL, NULL),
('PR00002', 'Indonesia Mundur', 'Bandung', '2018-12-21', NULL, '2019-12-21', NULL, 'Progress', '', 0, '10001', NULL, NULL, NULL, NULL),
('PR00003', 'Indonesia Mundur', 'Bandung', '2018-12-21', NULL, '2019-12-21', NULL, 'Progress', '', 0, '10001', NULL, NULL, NULL, NULL),
('PR00004', 'Hidup Indonesia', 'Jakarta', '2018-12-08', NULL, '2018-12-15', NULL, 'Progress', NULL, 0, '10001', NULL, NULL, NULL, NULL),
('PR00005', 'Indonesiaku', 'Bandung', '2018-12-14', NULL, '2018-12-28', NULL, 'Progress', NULL, 0, '10001', NULL, NULL, NULL, NULL),
('PR00006', 'Test', 'Test', '2019-01-08', NULL, '2019-01-31', NULL, 'Progress', NULL, 0, '10001', NULL, NULL, NULL, NULL),
('PR00007', 'Test', 'Test', '2019-01-15', NULL, '2019-01-31', NULL, 'Progress', 'testing', 0, '10001', NULL, NULL, NULL, NULL),
('PR00008', 'Indonesia', 'Jakarta', '2019-01-09', NULL, '2019-01-31', NULL, 'Progress', 'test 2', 0, '10001', NULL, NULL, NULL, NULL),
('PR00009', 'Malaysia', 'Indonesia', '2019-01-16', NULL, '2019-01-25', NULL, 'Progress', 'terakhir', 0, '10001', NULL, NULL, NULL, NULL),
('PR00010', 'Test', 'Test', '2019-01-08', NULL, '2019-01-23', NULL, 'Progress', '', 0, '10001', NULL, NULL, NULL, NULL),
('PR00011', 'Test', 'Test', '2019-01-08', NULL, '2019-01-23', NULL, 'Progress', 'test', 0, '10001', NULL, NULL, NULL, NULL),
('PR00012', 'Test', 'Tsets', '2019-01-08', NULL, '2019-01-19', NULL, 'Progress', '', 0, '10001', NULL, NULL, NULL, NULL),
('PR00013', 'Test', 'Test', '2019-01-08', NULL, '2019-01-23', NULL, 'Progress', '', 0, '10001', NULL, NULL, NULL, NULL),
('PR00014', 'Tst', 'Test', '2019-01-08', NULL, '2019-01-29', NULL, 'Progress', '', 0, '10001', NULL, NULL, NULL, NULL),
('PR00015', 'Test', 'Test', '2019-01-16', NULL, '2019-01-23', NULL, 'Progress', '', 0, '10001', NULL, NULL, NULL, NULL),
('PR00016', 'Test', 'Test', '2019-01-09', NULL, '2019-01-31', NULL, 'Progress', '', 0, '10001', NULL, NULL, NULL, NULL),
('PR00017', 'Test', 'Test', '2019-01-08', NULL, '2019-01-30', NULL, 'Progress', '', 0, '10001', NULL, NULL, NULL, NULL),
('PR00018', 'Test', 'Tet', '2019-01-09', NULL, '2019-01-31', NULL, 'Progress', '', 0, '10001', NULL, NULL, NULL, NULL),
('PR00019', 'Test', 'Test', '2019-01-10', NULL, '2019-01-24', NULL, 'Progress', '', 0, '10001', NULL, NULL, NULL, NULL),
('PR00020', 'Test', 'Test', '2019-01-08', NULL, '2019-01-08', NULL, 'Progress', '', 0, '10002', NULL, NULL, '2019-01-08', NULL),
('PR00021', 'Gojek', 'Jakarta', '2019-01-09', '2019-01-11', '2019-01-29', '2019-01-30', 'Closed', 'close 1', 0, '10001', NULL, '10001', '2019-01-10', NULL),
('PR00022', 'Grab', 'Bandung', '2019-01-09', NULL, '2019-01-31', NULL, 'Progress', 'Papa Mama', 4500000, '10001', NULL, NULL, NULL, NULL),
('PR00023', 'Mama', 'Papa', '2019-01-17', '2019-01-10', '2019-01-24', '2019-01-25', 'Progress', '', 0, '10001', '2019-01-08', '10001', NULL, NULL),
('PR00024', 'Semeru', 'Malang', '2019-01-12', NULL, '2019-01-12', NULL, 'Cancel', 'parah', 0, '10003', '2019-01-08', NULL, '2019-01-10', '10001'),
('PR00025', 'Gojek', 'Ojol', '2019-01-11', NULL, '2019-01-19', NULL, 'Progress', 'test', 28550000, '10001', '2019-01-10', NULL, '2019-01-10', NULL);

--
-- Triggers `tbl_proyek`
--
DELIMITER $$
CREATE TRIGGER `insert_contracts` AFTER INSERT ON `tbl_proyek` FOR EACH ROW BEGIN

insert into tbl_kontrak(kd_kontrak, kd_proyek, no_kontrak, desk_kontrak, kd_customer, nilai_kontrak, deadline) select * from tmp_kontrak where kd_proyek=New.kd_proyek;

DELETE from tmp_kontrak where kd_proyek=New.kd_proyek;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rab`
--

CREATE TABLE `tbl_rab` (
  `kd_rab` varchar(10) NOT NULL,
  `kd_proyek` varchar(7) NOT NULL,
  `tgl_rab` date NOT NULL,
  `total_rab` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` char(5) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(50) NOT NULL,
  `id_position` char(5) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `first_name`, `last_name`, `username`, `password`, `id_position`, `email`) VALUES
('10001', 'Administrator', 'Administrator', 'admin', 'admin', 'P01', 'riyanalfian93@gmail.com'),
('10002', 'user', 'user', 'user', 'user', 'P03', 'riyanalfian93@gmail.com'),
('10003', 'Wawan', 'Yusuf', 'wawan123', 'indonesia', 'P01', 'wawan@gmai.com'),
('10004', 'Riyan', 'Alfian', 'riyan93', 'advance123', 'P01', 'riyan@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `kd_kontrak` varchar(7) NOT NULL,
  `kd_proyek` varchar(7) NOT NULL,
  `no_kontrak` varchar(20) NOT NULL,
  `desk_kontrak` varchar(100) NOT NULL,
  `kd_customer` varchar(5) NOT NULL,
  `nilai_kontrak` double NOT NULL,
  `deadline` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_kontrak`
--

CREATE TABLE `tmp_kontrak` (
  `kd_kontrak` varchar(7) NOT NULL,
  `kd_proyek` varchar(7) NOT NULL,
  `no_kontrak` varchar(20) NOT NULL,
  `desk_kontrak` varchar(100) NOT NULL,
  `kd_customer` varchar(5) NOT NULL,
  `nilai_kontrak` double NOT NULL,
  `deadline` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_akun`
--
ALTER TABLE `tbl_akun`
  ADD PRIMARY KEY (`kd_akun`);

--
-- Indexes for table `tbl_biaya`
--
ALTER TABLE `tbl_biaya`
  ADD PRIMARY KEY (`kd_biaya`),
  ADD KEY `fk_biaya_project` (`kd_proyek`) USING BTREE;

--
-- Indexes for table `tbl_detail_biaya`
--
ALTER TABLE `tbl_detail_biaya`
  ADD PRIMARY KEY (`kd_detail_biaya`),
  ADD KEY `fk_dtlbiaya_biaya` (`kd_biaya`) USING BTREE,
  ADD KEY `fk_dtlbiaya_akun` (`kd_akun`) USING BTREE;

--
-- Indexes for table `tbl_detail_rab`
--
ALTER TABLE `tbl_detail_rab`
  ADD PRIMARY KEY (`kd_detail_rab`),
  ADD KEY `fk_dtlrab_rab` (`kd_rab`) USING BTREE,
  ADD KEY `fk_dtlrab_akun` (`kd_akun`) USING BTREE;

--
-- Indexes for table `tbl_kontrak`
--
ALTER TABLE `tbl_kontrak`
  ADD PRIMARY KEY (`kd_kontrak`),
  ADD KEY `fk_kontrak_proyek` (`kd_proyek`),
  ADD KEY `fk_kontrak_customer` (`kd_customer`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `tbl_pelanggan`
--
ALTER TABLE `tbl_pelanggan`
  ADD PRIMARY KEY (`kd_customer`);

--
-- Indexes for table `tbl_pendapatan`
--
ALTER TABLE `tbl_pendapatan`
  ADD PRIMARY KEY (`kd_pendapatan`),
  ADD KEY `fk_pendapatan_kontrak` (`kd_kontrak`);

--
-- Indexes for table `tbl_position`
--
ALTER TABLE `tbl_position`
  ADD PRIMARY KEY (`id_position`);

--
-- Indexes for table `tbl_proyek`
--
ALTER TABLE `tbl_proyek`
  ADD PRIMARY KEY (`kd_proyek`);

--
-- Indexes for table `tbl_rab`
--
ALTER TABLE `tbl_rab`
  ADD PRIMARY KEY (`kd_rab`),
  ADD KEY `fk_rab_proyek` (`kd_proyek`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tmp_kontrak`
--
ALTER TABLE `tmp_kontrak`
  ADD PRIMARY KEY (`kd_kontrak`),
  ADD KEY `kd_customer` (`kd_customer`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_biaya`
--
ALTER TABLE `tbl_biaya`
  ADD CONSTRAINT `fk_project` FOREIGN KEY (`kd_proyek`) REFERENCES `tbl_proyek` (`kd_proyek`);

--
-- Constraints for table `tbl_detail_biaya`
--
ALTER TABLE `tbl_detail_biaya`
  ADD CONSTRAINT `fk_akun` FOREIGN KEY (`kd_akun`) REFERENCES `tbl_akun` (`kd_akun`),
  ADD CONSTRAINT `fk_biaya` FOREIGN KEY (`kd_biaya`) REFERENCES `tbl_biaya` (`kd_biaya`);

--
-- Constraints for table `tbl_detail_rab`
--
ALTER TABLE `tbl_detail_rab`
  ADD CONSTRAINT `fk_rab` FOREIGN KEY (`kd_rab`) REFERENCES `tbl_rab` (`kd_rab`),
  ADD CONSTRAINT `fk_rab_akun` FOREIGN KEY (`kd_akun`) REFERENCES `tbl_akun` (`kd_akun`);

--
-- Constraints for table `tbl_kontrak`
--
ALTER TABLE `tbl_kontrak`
  ADD CONSTRAINT `fk_kontrak_customer` FOREIGN KEY (`kd_customer`) REFERENCES `tbl_pelanggan` (`kd_customer`),
  ADD CONSTRAINT `fk_kontrak_proyek` FOREIGN KEY (`kd_proyek`) REFERENCES `tbl_proyek` (`kd_proyek`);

--
-- Constraints for table `tbl_pendapatan`
--
ALTER TABLE `tbl_pendapatan`
  ADD CONSTRAINT `fk_pendapatan_kontrak` FOREIGN KEY (`kd_kontrak`) REFERENCES `tbl_kontrak` (`kd_kontrak`);

--
-- Constraints for table `tbl_rab`
--
ALTER TABLE `tbl_rab`
  ADD CONSTRAINT `fk_rab_proyek` FOREIGN KEY (`kd_proyek`) REFERENCES `tbl_proyek` (`kd_proyek`);

--
-- Constraints for table `tmp_kontrak`
--
ALTER TABLE `tmp_kontrak`
  ADD CONSTRAINT `tmp_kontrak_ibfk_2` FOREIGN KEY (`kd_customer`) REFERENCES `tbl_pelanggan` (`kd_customer`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
